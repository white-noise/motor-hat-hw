EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "4 Channel servo Controller"
Date ""
Rev ""
Comp ""
Comment1 "White noise"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3900 3500 4500 3500
Wire Wire Line
	4500 3500 4500 4350
Wire Wire Line
	3900 4150 4250 4150
Wire Wire Line
	4250 4150 4250 4450
Wire Wire Line
	4250 4450 4500 4450
Wire Wire Line
	3900 4800 4250 4800
Wire Wire Line
	4250 4800 4250 4550
Wire Wire Line
	4250 4550 4500 4550
Wire Wire Line
	3900 5450 4500 5450
Wire Wire Line
	4500 5450 4500 4650
Wire Wire Line
	5300 4450 5750 4450
Wire Wire Line
	5750 4450 5750 3700
Wire Wire Line
	5750 3700 6050 3700
Wire Wire Line
	5300 4550 5900 4550
Wire Wire Line
	5900 4550 5900 3800
Wire Wire Line
	5900 3800 6050 3800
$Comp
L power:GND #PWR012
U 1 1 61DF3A6C
P 5300 4650
F 0 "#PWR012" H 5300 4400 50  0001 C CNN
F 1 "GND" H 5305 4477 50  0000 C CNN
F 2 "" H 5300 4650 50  0001 C CNN
F 3 "" H 5300 4650 50  0001 C CNN
	1    5300 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 61DF3A8C
P 4200 5900
F 0 "#PWR08" H 4200 5650 50  0001 C CNN
F 1 "GND" H 4205 5727 50  0000 C CNN
F 2 "" H 4200 5900 50  0001 C CNN
F 3 "" H 4200 5900 50  0001 C CNN
	1    4200 5900
	1    0    0    -1  
$EndComp
$Sheet
S 7200 3700 1000 1050
U 61E0354C
F0 "Sheet61E0354B" 50
F1 "powersystem.sch" 50
$EndSheet
NoConn ~ 6050 3900
NoConn ~ 6550 3900
NoConn ~ 6550 4000
NoConn ~ 6550 4100
NoConn ~ 6550 4200
NoConn ~ 6550 4300
NoConn ~ 6550 4400
NoConn ~ 6550 4500
NoConn ~ 6550 4600
NoConn ~ 6550 4700
NoConn ~ 6550 4800
NoConn ~ 6550 4900
NoConn ~ 6550 5000
NoConn ~ 6550 5100
NoConn ~ 6550 5200
NoConn ~ 6550 5300
NoConn ~ 6550 5400
NoConn ~ 6550 5500
NoConn ~ 6050 5500
NoConn ~ 6050 5400
NoConn ~ 6050 5300
NoConn ~ 6050 5200
NoConn ~ 6050 5100
NoConn ~ 6050 5000
NoConn ~ 6050 4900
NoConn ~ 6050 4800
NoConn ~ 6050 4700
NoConn ~ 6050 4600
NoConn ~ 6050 4500
NoConn ~ 6050 4400
NoConn ~ 6050 4300
NoConn ~ 6050 4200
NoConn ~ 6050 4100
NoConn ~ 6050 4000
$Comp
L power:+5V #PWR015
U 1 1 61E1E150
P 6750 3400
F 0 "#PWR015" H 6750 3250 50  0001 C CNN
F 1 "+5V" V 6765 3528 50  0000 L CNN
F 2 "" H 6750 3400 50  0001 C CNN
F 3 "" H 6750 3400 50  0001 C CNN
	1    6750 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 61E1E195
P 6750 3950
F 0 "#PWR016" H 6750 3700 50  0001 C CNN
F 1 "GND" V 6755 3822 50  0000 R CNN
F 2 "" H 6750 3950 50  0001 C CNN
F 3 "" H 6750 3950 50  0001 C CNN
	1    6750 3950
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR011
U 1 1 61E465F8
P 5400 4200
F 0 "#PWR011" H 5400 4050 50  0001 C CNN
F 1 "VDD" V 5417 4328 50  0000 L CNN
F 2 "" H 5400 4200 50  0001 C CNN
F 3 "" H 5400 4200 50  0001 C CNN
	1    5400 4200
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR07
U 1 1 61E4666A
P 4050 3250
F 0 "#PWR07" H 4050 3100 50  0001 C CNN
F 1 "VDD" V 4067 3378 50  0000 L CNN
F 2 "" H 4050 3250 50  0001 C CNN
F 3 "" H 4050 3250 50  0001 C CNN
	1    4050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3600 4050 3600
Wire Wire Line
	3900 4250 4050 4250
Wire Wire Line
	4050 4250 4050 3600
Connection ~ 4050 3600
Wire Wire Line
	3900 4900 4050 4900
Wire Wire Line
	4050 4900 4050 4250
Connection ~ 4050 4250
Wire Wire Line
	3900 5550 4050 5550
Wire Wire Line
	4050 5550 4050 4900
Connection ~ 4050 4900
Wire Wire Line
	3900 3700 4200 3700
Wire Wire Line
	4200 3700 4200 4350
Wire Wire Line
	4050 3250 4050 3600
Wire Wire Line
	3900 4350 4200 4350
Connection ~ 4200 4350
Wire Wire Line
	4200 4350 4200 5000
Wire Wire Line
	3900 5000 4200 5000
Connection ~ 4200 5000
Wire Wire Line
	4200 5000 4200 5650
Wire Wire Line
	3900 5650 4200 5650
Connection ~ 4200 5650
Wire Wire Line
	4200 5650 4200 5900
Wire Wire Line
	6750 3800 6750 3950
Wire Wire Line
	5300 4350 5400 4350
Wire Wire Line
	5400 4350 5400 4200
$Comp
L payload:Connector_Generic_Conn_02x20_Odd_Even-raspberrypi_zerow_uhat-cache J1
U 1 1 61E9D75A
P 6250 4600
F 0 "J1" H 6300 5717 50  0000 C CNN
F 1 "Connector_Generic_Conn_02x20_Odd_Even-raspberrypi_zerow_uhat-cache" H 6300 5650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 6250 4600 50  0001 C CNN
F 3 "" H 6250 4600 50  0001 C CNN
	1    6250 4600
	1    0    0    -1  
$EndComp
$Comp
L payload:PCA9633DP1,118-payloadlibrary U5
U 1 1 61E9D7D7
P 4900 4550
F 0 "U5" H 4900 4975 50  0000 C CNN
F 1 "PCA9633DP1,118-payloadlibrary" H 4900 4884 50  0000 C CNN
F 2 "eec:NXP_Semiconductors-PCA9633DP1,118-Level_A" H 4750 4400 50  0001 C CNN
F 3 "" H 4750 4400 50  0001 C CNN
	1    4900 4550
	1    0    0    -1  
$EndComp
$Comp
L payload:servo-payloadlibrary U1
U 1 1 61E9D862
P 3700 3650
F 0 "U1" H 3731 4025 50  0000 C CNN
F 1 "servo-payloadlibrary" H 3731 3934 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 3700 3650 50  0001 C CNN
F 3 "" H 3700 3650 50  0001 C CNN
	1    3700 3650
	1    0    0    -1  
$EndComp
$Comp
L payload:servo-payloadlibrary U2
U 1 1 61E9D88A
P 3700 4300
F 0 "U2" H 3731 4675 50  0000 C CNN
F 1 "servo-payloadlibrary" H 3731 4584 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 3700 4300 50  0001 C CNN
F 3 "" H 3700 4300 50  0001 C CNN
	1    3700 4300
	1    0    0    -1  
$EndComp
$Comp
L payload:servo-payloadlibrary U3
U 1 1 61E9D8F8
P 3700 4950
F 0 "U3" H 3731 5325 50  0000 C CNN
F 1 "servo-payloadlibrary" H 3731 5234 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 3700 4950 50  0001 C CNN
F 3 "" H 3700 4950 50  0001 C CNN
	1    3700 4950
	1    0    0    -1  
$EndComp
$Comp
L payload:servo-payloadlibrary U4
U 1 1 61E9D922
P 3700 5600
F 0 "U4" H 3731 5975 50  0000 C CNN
F 1 "servo-payloadlibrary" H 3731 5884 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 3700 5600 50  0001 C CNN
F 3 "" H 3700 5600 50  0001 C CNN
	1    3700 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3800 6750 3800
Wire Wire Line
	6750 3700 6550 3700
Wire Wire Line
	6750 3400 6750 3700
NoConn ~ 6050 5600
NoConn ~ 6550 5600
$EndSCHEMATC
